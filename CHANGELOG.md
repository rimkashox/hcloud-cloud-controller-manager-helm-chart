# Changelog

## v2.4.1

  * Fixed Chart icon URL (broken due to branch renaming)

## v2.4.0

  * Upgraded hcloud-cloud-controller-manager to v1.11.1
  * Added support for `HCLOUD_INSTANCES_ADDRESS_FAMILY` (introduced by hcloud-cloud-controller-manager v1.11.0)
  * Added support for `HCLOUD_NETWORK_DISABLE_ATTACHED_CHECK` (introduced by hcloud-cloud-controller-manager v1.11.0)
  * Fixed indention bug (#6)

## v2.3.0

  * Upgraded hcloud-cloud-controller-manager to v1.10.0
  * Support for LoadBalancer defaults (introduced by hcloud-cloud-controller-manager v1.9.0)

## v2.2.2

  * Fixed `helm lint` warnings regarding `ClusterRoleBinding`

## v2.2.1

  * Upgraded hcloud-cloud-controller-manager to v1.8.1

## v2.2.0

  * Upgraded hcloud-cloud-controller-manager to v1.8.0
  * Fixed naming interference with hcloud-csi-driver helm chart (#3)
  * Moved network plugin hints to Wiki

## v2.1.0

  * Upgraded hcloud-cloud-controller-manager to v1.7.0
  * Added support for setting `HCLOUD_DEBUG`
  * Added support for enabling/disabling load balancers support

## v2.0.3

  * Added logo/icon

## v2.0.2

  * Documentation fixes
    * use namespace `kube-system` by default
    * example how to reuse existing secret

## v2.0.1

  * Fixed example usage documentation: `token` -> `hcloudApiToken`

## v2.0.0

  * Added hints in documentation for allowing coredns and network plugin to use unitizialized nodes
  * Updated Helm Chart keywords
  * Rewrite of values.yaml structure
  * Allow labels/annotations for all objects to be created (#1)
  * Allow for reusing existing Secret

## v1.0.1

  * Added NOTES.txt

## v1.0.0

  * First working Chart version
